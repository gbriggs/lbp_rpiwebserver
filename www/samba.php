﻿<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Samba Setup</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <link rel="stylesheet" type="text/css" href="./styles/default_style.css" />
</head>

<body>
    <div class="page">
        <div class="header">
            <div class="title">
                <header>
                <titletext>Raspberry Pi Web Server</titletext>
                <!-- Header Image -->
                <img id="headerimage" src="./Images/rpiraspberries.png" />
                </header>
            </div>
        </div>
        
    <div class="content">
<h2>Samba Setup</h2>
<p>Samba file sharing setup:</p>
<pre>user name:  root, pi</pre>
<pre>password:  raspberry</pre>

<p>Directories:</p>
<pre>pi:  /home/pi</pre>
<pre>web_root: /usr/share/nginx</pre>

</div>
</body>
</html>
