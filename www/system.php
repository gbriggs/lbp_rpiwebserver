<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>System</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <link rel="stylesheet" type="text/css" href="./styles/default_style.css" />
</head>

<body>
    <div class="page">
        <div class="header">
            <div class="title">
                <header>
                <titletext>Raspberry Pi Web Server</titletext>
                <!-- Header Image -->
                <img id="headerimage" src="./Images/rpiraspberries.png" />
                </header>
            </div>
        </div>
        


        <div class="content">
        <h2>System Information</h2>
            <?php
$cpu1 = shell_exec('grep processor /proc/cpuinfo');
    echo "<pre>$cpu1</pre>";
?>



<?php
	echo "<h3>Memory</h3>";
$memory = shell_exec('free -htl');
    echo "<pre>$memory</pre>";

    echo "<h3>Disk Usage</h3>";
    $disk = shell_exec('df -h');
    echo "<pre>$disk</pre>"
?>

    </br>
    </br>
    </br>
    
    <h3>Reboot / Shutdown</h3>
  
<input type="button" id="reboot"  value="Reboot" onClick="reboot()"/> 
<input type="button" id="reboot"  value="Shut Down" onClick="shutdown()"/> 
 

<script>  
  function reboot() {  
       window.location="system.php?status=reboot";  
  }  
  function shutdown() {  
      window.location="system.php?status=shutdown";  
 }  
  
</script>  
   
   
   <?php  
   $status = $_GET["status"];  
  
  if ($status == "reboot")
  {  
       exec("python /usr/share/nginx/www/reboot.py");  
       print '<h2>Server is rebooting ...</h2>';  
  }  
  else if ($status == "shutdown")
  {
  		exec("python /usr/share/nginx/www/shutdown.py");
  		print '<h2>Server is shutting down ...</h2>';
  }
  
?>  
 
        </div>
    </div></body>
</html>
