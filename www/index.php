<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>RPi Web Server</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <link rel="stylesheet" type="text/css" href="./styles/default_style.css" />
</head>

<body>
    <div class="page">
        <div class="header">
            <div class="title">
                <header>
                <titletext>Raspberry Pi Web Server</titletext>
                <!-- Header Image -->
                <img id="headerimage" src="./Images/rpiraspberries.png" />
                </header>
            </div>
        </div>
        


        <div class="content">
        <h2>PHP Application on Default</h2>
            <p>You are looking at the index page in the default Nginx web root at: <pre>/usr/share/nginx/www</pre></p>
            <p>This site is configured to use PHP, and is ready for you to deploy your .php files to the web root directory.</p>
            
            <ul>
            <li><a href="./info.php">PHP Version Information</a></li>
            <li><a href="./phpmyadmin.php">phpMyAdmin Login Information</a></li>
            </ul>
            <h2>C# Applications using ASP.NET</h2>
            <p>There is a web server on port 30080 serving an ASP.NET web application using the Mono C# runtime framework.</p>
            <ul>
            <li><a href="#" onclick="javascript:window.location.port=30080">ASP.NET Hello World on port 30080</a></li></ul>

            <h2>System Information</h2>
            <ul>
            <li><a href="./system.php">System Information</a></li>
                <li><a href="./samba.php">Samba File Sharing</a></li>
                <li><a href="./vnc.php">VNC Screen Sharing</a></li>
            </ul>

        </div>
    </div></body>
</html>
