﻿<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>VNC Setup</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <link rel="stylesheet" type="text/css" href="./styles/default_style.css" />
</head>

<body>
    <div class="page">
        <div class="header">
            <div class="title">
                <header>
                <titletext>Raspberry Pi Web Server</titletext>
                <!-- Header Image -->
                <img id="headerimage" src="./Images/rpiraspberries.png" />
                </header>
            </div>
        </div>
        
        
<div class="content">
<h2>VNC Setup</h2>
<p>Screen Sharing via VNC configured on port 5900</p>
<pre>password:  raspberry</pre>

<p><h3>VNC Status</h3><p>

<?php
	
$pid = shell_exec('pidof x11vnc');
    if ( $pid == "" )
    	echo "<p>VNC is not running</p>";
    else 
    	echo "<p>VNC is running - pid : $pid</p>";
    ?>

</div>
</body>
</html>
